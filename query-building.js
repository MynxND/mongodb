const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function main () {
  const room = await Room.findById('6216424588f4a52d925db922')
  room.capacity = 10
  room.save()
  const rooms = await Room.find({ capacity: { $gte: 100 } })
  const rooms = await Room.find({ capacity: { $gte: 100 } })
  console.log(rooms)

  console.log('**********************************')

  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
}

main().then(function () {
  console.log('Finish')
})
